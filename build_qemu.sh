#!/bin/bash

# variables that may need updating
QEMU_REPO=/opt/repos/qemu
GHIDRA_VERSION=ghidra_9.2_DEV

echo Please run the script as "sudo"
sudo echo Thanks!

#apt_packages() {
#  echo ">>> Installing dependencies"
#  sudo apt install curl unzip git flex bison openjdk-11-jdk openjdk-11-jre
#}

yum_packages() {
  echo ">>> Installing dependencies"
  sudo yum install curl unzip git flex bison java-11-openjdk.x86_64 java-11-openjdk-devel.x86_64 
  # enable the binfmt_misc
  sudo echo 1 > /proc/sys/fs/binfmt_misc/status
}

install_gradle() {
  # Installing gradle if not found
  if [ ! -d $GRADLE_FOLDER ]; then
    echo ">>> Installing gradle"
    sudo curl https://downloads.gradle-dn.com/distributions/$GRADLE_VERSION-bin.zip >> /tmp/$GRADLE_VERSION-bin.zip
    sudo unzip -d $GRADLE_FOLDER /tmp/gradle-*.zip
  fi

  # set an env variable in bash if needed
  if [ ! -x "$(command -v gradle)" ]; then
    echo ">>> Set a env variable for gradle"
    bash -c "cat >> ~/.bashrc << EOF 
export PATH=$PATH:$GRADLE_FOLDER/$GRADLE_VERSION/bin 
EOF
"
    sudo chown -R $USERNAME:$USERNAME $GRADLE_FOLDER 
    source ~/.bashrc 
  fi
}

download_repo() {
  # create the folder and clone the directory if not present
  if [ ! -d $QEMU_REPO ]; then
    echo ">> Cloning QEMU"
    git clone https://github.com/qemu/qemu.git $QEMU_REPO;
    cd $QEMU_REPO;
  elif [ -d $QEMU_REPO ]; then
    # if preset, just pull the repo
    cd $QEMU_REPO;
    git pull;
  fi
}

build_source() {
  echo ">>> Build QEMU"
  # need to source the newly create PATH fo this func

  cd $QEMU_REPO;
  sudo ./configure
  # By default, the make file will build for all supported platforms
  # By default, QEMU will be built to be placed in /usr/local/bin 
  sudo make
}

install_ghidra() {
  echo ">>> Install the new ghidra zip file"
  # unzip the new build or exit
  FILE=$GHIDRA_REPO/build/dist/*zip
  if [ -f $FILE ]; then
    sudo unzip -d $GHIDRA_FOLDER $GHIDRA_REPO/build/dist/*zip
  else
    echo "Error: Ghidra repo directory not present"
    exit 1
  fi

  # delete teh icon if it exists
  ICON=~/Desktop/Ghidra.desktop
  if [ -f $FILE ]; then
    sudo rm ~/Desktop/Ghidra.desktop
  fi

  # updated the permissions
  sudo chown -R $USERNAME:$USERNAME $GHIDRA_FOLDER 
  
  echo ">>> Set up the desktop icon"
  bash -c "cat > ~/Desktop/Ghidra.desktop << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=Ghidra
Comment=
Exec=$GHIDRA_FOLDER/$GHIDRA_VERSION/ghidraRun
Icon=$GHIDRA_FOLDER/$GHIDRA_VERSION/docs/GhidraClass/Advanced/ghidraRight.png
Path=
Terminal=false
StartupNotify=false
EOF
"
  sudo chmod 775 ~/Desktop/Ghidra.desktop
  sudo chown $USERNAME:$USERNAME ~/Desktop/Ghidra.desktop
}

build_qemu() {
  #apt_packages
  #yum_packages
  #install_gradle
  download_repo
  #build_source
  #install_ghidra
}

# build the ghidra repo and then install the zip file
build_qemu
