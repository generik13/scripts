#!/bin/bash

# variables that may need updating
GRADLE_VERSION=gradle-5.6.3
GRADLE_FOLDER=/opt/gradle
GHIDRA_FOLDER=/opt/ghidra
GHIDRA_REPO=/opt/repos/ghidra
GHIDRA_VERSION=ghidra_9.2_DEV

echo Please run the script as "sudo"
sudo echo Thanks!

apt_packages() {
  echo ">>> Installing dependencies"
  sudo apt install curl unzip git flex bison openjdk-11-jdk openjdk-11-jre
  sudo alternatives --config java <<< '3'
  sudo alternatives --config javac <<< '1'
}

install_gradle() {
  # Installing gradle if not found
  if [ ! -d $GRADLE_FOLDER ]; then
    echo ">>> Installing gradle"
    sudo curl https://downloads.gradle-dn.com/distributions/$GRADLE_VERSION-bin.zip >> /tmp/$GRADLE_VERSION-bin.zip
    sudo unzip -d $GRADLE_FOLDER /tmp/gradle-*.zip
  fi

  # set an env variable in bash if needed
  if ! ($(echo $PATH | grep gradle)) ; then
    echo ">>> Adding gradle to $PATH"
    echo ">>> Set a env variable for gradle"
    bash -c "cat >> ~/.bashrc << EOF 
export PATH=$PATH:$GRADLE_FOLDER/$GRADLE_VERSION/bin 
EOF
"
    sudo chown -R $USERNAME:$USERNAME $GRADLE_FOLDER 
    source ~/.bashrc 
  fi
}

download_repo() {
  # create the folder and clone the directory if not present
  if [ ! -d $GHIDRA_REPO ]; then
    echo ">> Cloning Ghidra"
    git clone https://github.com/NationalSecurityAgency/ghidra.git /opt/repos/ghidra;
    cd $GHIDRA_REPO;
  elif [ -d $GHIDRA_REPO ]; then
    # if preset, just pull the repo
    cd $GHIDRA_REPO;
    git pull;
  fi
}

build_source() {
  echo ">>> Build Ghidra"
  # need to source the newly create PATH fo this func
  source ~/.bashrc 

  cd $GHIDRA_REPO;

  # Remove the older releases
  FILE=$GHIDRA_REPO/build/dist/*zip
  if [ -f $FILE ]; then
    sudo rm -rf $FILE
  fi

  # Automatic Script Instructions
  # setup automatically the flat directory-style repository
  gradle --init-script gradle/support/fetchDependencies.gradle init

  # build Ghidra
  gradle buildGhidra

  # building the natives
  gradle buildNatives_linux64
  gradle sleighCompile

  # run unit tests (optional)
  #gradle combinedTestReport
}

install_ghidra() {
  echo ">>> Install the new ghidra zip file"
  # unzip the new build or exit
  FILE=$GHIDRA_REPO/build/dist/*zip
  if [ -f $FILE ]; then
    sudo unzip -d $GHIDRA_FOLDER $GHIDRA_REPO/build/dist/*zip
  else
    echo "Error: Ghidra repo directory not present"
    exit 1
  fi

  # delete teh icon if it exists
  ICON=~/Desktop/Ghidra.desktop
  if [ -f $FILE ]; then
    sudo rm ~/Desktop/Ghidra.desktop
  fi

  # updated the permissions
  sudo chown -R $USERNAME:$USERNAME $GHIDRA_FOLDER 
  
  echo ">>> Set up the desktop icon"
  bash -c "cat > ~/Desktop/Ghidra.desktop << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=Ghidra
Comment=
Exec=$GHIDRA_FOLDER/$GHIDRA_VERSION/ghidraRun
Icon=$GHIDRA_FOLDER/$GHIDRA_VERSION/docs/GhidraClass/Advanced/ghidraRight.png
Path=
Terminal=false
StartupNotify=false
EOF
"
  sudo chmod 775 ~/Desktop/Ghidra.desktop
  sudo chown $USERNAME:$USERNAME ~/Desktop/Ghidra.desktop
}

build_ghidra() {
  #apt_packages
  yum_packages
  install_gradle
  download_repo
  build_source
  install_ghidra
}

# build the ghidra repo and then install the zip file
build_ghidra
